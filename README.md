# jmeter_proj

http://127.0.0.1:8000/jmeter_api/jmx_file

form-data

jmx_name : ["name1", "name2", "name3"]
api_name : "Test 1"
scenario : "Secanrio Desc"
spreadsheet (type: file) : choose file to upload

______________________________________________________________________________________________________________________________________

http://127.0.0.1:8000/jmeter_api/jm_update

JSON

{
    "api_name":"name",
    "duration":"41",
    "p1":"1",
    "p2":"1",
    "p3":"1"
}

curl -X POST -H "Content-Type: application/json" http://127.0.0.1:8000/jmeter_api/jm_update -d "{\"api_name\":\"name\",\"duration\":\"41\",\"p1\":\"1\",\"p2\":\"1\",\"p3\":\"1\"}"

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/venkatesh.gandha/jmeter_proj.git
git branch -M main
git push -uf origin main
```
