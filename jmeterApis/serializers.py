from django.contrib.auth.models import User, Group
from .models import Jmeter, JmxFile
from rest_framework import serializers

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'group']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class JmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Jmeter
        fields = ['api_name', 'duration', 'p1', 'p2', 'p3']

class JmFileUpdate(serializers.ModelSerializer):
    jmx_name = serializers.ListField(child=serializers.CharField())
    spreadsheet = serializers.FileField(max_length=None, allow_empty_file=False)
    class Meta:
        model = JmxFile
        fields = ['jmx_name', 'api_name', 'scenario', 'spreadsheet']