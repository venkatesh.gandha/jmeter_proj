# basic URL Configurations
from django.urls import include, path

# import everything from views
from .views import *
 
# specify URL Path for rest_framework
urlpatterns = [    
    path('jm_update', JmFileUpdate.as_view()),
    path('jmx_file', UpdatedJmFile.as_view())  
    ]