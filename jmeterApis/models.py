from django.db import models
from django.contrib.auth.models import User

class Jmeter(models.Model):
    api_name = models.CharField(max_length=100)
    duration = models.CharField(max_length=100)
    p1 = models.CharField(max_length=100)
    p2 = models.CharField(max_length=100)
    p3 = models.CharField(max_length=100)

    def __str__(self):
        return self.api_name

class JmxFile(models.Model):
    jmx_name = models.TextField()
    api_name = models.CharField(max_length=250)
    scenario = models.CharField(max_length=200)
    spreadsheet = models.FileField()
    def __str__(self):
        return self.api_name