from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser
from .serializers import JmSerializer, JmFileUpdate
from .models import Jmeter
import gitlab
import yaml
import json

class JmFileUpdate(APIView):
    def post(self, request):
        serializer = JmSerializer(data=request.data)
        if serializer.is_valid():
            with open('commits.yml') as f: 
                try:               
                    doc = yaml.safe_load(f)
            
                    for i in doc:
                        if i['api_name'] == serializer.data['api_name']:
                            i['duration'] = serializer.data['duration']
                            i['p1'] = serializer.data['p1']
                            i['p2'] = serializer.data['p2']
                            i['p3'] = serializer.data['p3']

                            with open('commits.yml', 'w') as f:
                                yaml.dump(doc, f)
                                updatingGit('commits.yml')
                                
                            # serializer.save()
                            return Response({"status": "success", "data": "{} is updated".format(i['api_name'])}, status=status.HTTP_204_NO_CONTENT)
                except yaml.YAMLError as exc:        
                    return Response({"status": "error", "data": exc}, status=status.HTTP_400_BAD_REQUEST)

def updatingGit(filename):
    gl = gitlab.Gitlab(url='https://gitlab.com', private_token='glpat-nJhfzJRxPXEwCH6dnWae')
    gl.auth()
    project = gl.projects.get(42114440)
    
    items = project.repository_tree(path = 'yaml_file', ref = 'main', recursive=True, all=True)

    for f in items:
        if filename in f['name']:
            action = "update"
            break
        else:
            action = "create"

    data = {
        'branch' : 'main',
        'commit_message': 'updating {} '.format(filename),
        'actions': [
            {
                'action': action,
                'file_path': 'yaml_file/{}'.format(filename),
                'content': open(filename).read()
            }            

        ]
    }
    commit = project.commits.create(data)
    return commit

    # curl -X POST -H "Content-Type: application/json" http://127.0.0.1:8000/jmeter_api/jm_update -d "{\"api_name\":\"name\",\"duration\":\"41\",\"p1\":\"1\",\"p2\":\"1\",\"p3\":\"1\"}"

class UpdatedJmFile(APIView):
    parser_classes = (MultiPartParser,)
    def post(self, request):
        serializer_class = JmFileUpdate(data=request.data)

        jmx_name = serializer_class.data['jmx_name']
        api_name = serializer_class.data['api_name']
        scenario = serializer_class.data['scenario']
        spreadsheet = request.FILES.get('spreadsheet')
        content_type = spreadsheet.content_type

        f_name = json.loads(jmx_name)
        file_names = []
        for fn in f_name:
            file_names.append(str(fn) + "-" +str(api_name) + "-" + str(scenario))

        if 'spreadsheet' not in request.FILES:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            handle_uploaded_file(request.FILES['spreadsheet'])
            response = "Filenames created {}".format(file_names)
            return Response({'file_names': response, 'file_uploaded': "jmx_uploads/"+request.FILES['spreadsheet'].name},status=status.HTTP_201_CREATED)
        

def handle_uploaded_file(f):
    with open('jmx_uploads/'+f.name, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
        destination.close()

