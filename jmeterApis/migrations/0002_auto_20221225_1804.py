# Generated by Django 3.2.16 on 2022-12-25 12:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jmeterApis', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Jmeter',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('api_name', models.CharField(max_length=100)),
                ('duration', models.CharField(max_length=100)),
                ('p1', models.CharField(max_length=100)),
                ('p2', models.CharField(max_length=100)),
                ('p3', models.CharField(max_length=100)),
            ],
        ),
        migrations.DeleteModel(
            name='GeeksModel',
        ),
    ]
